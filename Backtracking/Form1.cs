﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Backtracking
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView.ColumnCount = 10;
            dataGridView.RowCount = 10;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void buttonProcess_Click(object sender, EventArgs e)
        {
            List<Point> list = new List<Point>();
            Utils.DataGridViewToList(dataGridView, list);
            Point[] points = list.ToArray();
            labelOut.Text = "Количество = " + Point.CountOfLines(points); ;
            labelOut.Enabled = true;
        }
    }
}
