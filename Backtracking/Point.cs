﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtracking
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool Connected { get; set; }


        public Point(int x, int y)
        {
            X = x;
            Y = y;
            Connected = false;
        }

        public Point()
        {
            X = 0;
            Y = 0;
            Connected = false;
        }

        public static int Connect(Point p1, Point p2, Point[] pArray)
        {
            int result = 0;
            if (p1.Connected == false)
            {
                result++;
            }
            if (p2.Connected == false)
            {
                result++;
            }
            foreach (Point p3 in pArray)
            {
                if (!p1.Equals(p3) && !p2.Equals(p3) && OnOneLine(p1, p2, p3))
                {
                    result++;
                }
            }
            return result;
        }

        public static bool OnOneLine(Point p1, Point p2, Point p3)
        {
            return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y) == 0;
        }

        public static int CountOfLines(Point[] points)
        {
            Point p1Max = new Point();
            Point p2Max = new Point();
            if (points.Length > 1)
            {
                int maxOfPoints = 0;
                for (int i = 0; i < points.Length; i++)
                {
                    for (int j = 0; j < points.Length; j++)
                    {
                        if (!points[i].Equals(points[j]) && Point.Connect(points[i], points[j], points) > maxOfPoints)
                        {
                            maxOfPoints = Point.Connect(points[i], points[j], points);
                            p1Max = points[i];
                            p2Max = points[j];
                        }
                    }
                }
                return 1 + CountOfLines(Point.UnconnectedPoints(p1Max, p2Max, points));
            }
            else
            {
                return points.Length;
            }
        }

        static Point[] UnconnectedPoints(Point p1, Point p2, Point[] pArray) 
        {
            List<Point> list = new List<Point>();
            foreach (Point p3 in pArray)
            {
                if (!p1.Equals(p3) && !p2.Equals(p3) && !OnOneLine(p1, p2, p3))
                {
                    list.Add(p3);    
                }
            }
            Point[] points = list.ToArray();
            return points;
        }
    }
}
