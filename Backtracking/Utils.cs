﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Backtracking
{
    public class Utils
    {
        public static void DataGridViewToList(DataGridView grid, List<Point> list)
        {
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                for (int j = 0; j < grid.RowCount; j++)
                {
                    if (grid[i, j].Value != null)
                    {
                        Point point = new Point(i + 1, grid.ColumnCount - j);
                        list.Add(point);
                    }
                }
            }
        }


    }
}
